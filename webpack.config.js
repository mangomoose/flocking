const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  output: {
    filename: process.env.VERSION ? `flocking.${process.env.VERSION}.js` : 'flocking.js',
    path: path.resolve(__dirname, 'dist'),
  },
};