# Flocking

A web based JavaScript flocking simulation library making use of a [web worker](https://developer.mozilla.org/en-US/docs/Web/API/Worker) to move calculations off of the main thread.

1. [Technology](#markdown-header-technology)
2. [Execution Steps](#markdown-header-execution-steps)
3. [Usage](#markdown-header-usage)
4. [API](#markdown-header-api)
5. [Classes](#markdown-header-classes)
6. [Config](#markdown-header-config)

## Technology

A [web worker](https://developer.mozilla.org/en-US/docs/Web/API/Worker) is used to move calculations off of the [main thread](https://developer.mozilla.org/en-US/docs/Glossary/Main_thread).

Data is transferred between the main thread and web worker using the [postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Worker/postMessage) function. By default this function uses the [structured clone algorithm](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm) to copy data between contexts which can be very slow. To get around this we use the optional transfer parameter of the web worker postMessage function to transfer ownership of data between the main thread and web worker.

Only objects that implement the [Transferable](https://developer.mozilla.org/en-US/docs/Web/API/Transferable) interface can be moved across contexts using the postMessage function. We use the [ArrayBuffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer) which is a fixed-length, raw binary data buffer which implements this interface. In order to correctly interact with this binary data we need to use an object that implements the [TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray) interface to interpret it. For this project we use a [Float32Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Float32Array) typed array to interpret our binary data as float values.

## Execution steps

A high level overview of the steps the framework executes during setup and each update cycle are listed below.

### Setup

1. Config is set in `window.fl.config`.
2. Main thread allocates the required memory for the `Float32Array` based on config data.
3. Main thread writes the current `Flocker` and `ForceField` state data into the `Float32Array`.

### Update cycle

1. Main thread transfers ownership of the `Float32Array` to worker thread.
2. Worker thread reads from `Float32Array` and updates its `Flocker` and `ForceField` objects with the latest state data.
3. Worker thread calculates new `Flocker` and `ForceField` data based on their current state, the rule set, and the config parameters.
4. Worker thread writes its `Flocker` and `ForceField` state data to `Float32Array`.
5. Worker thread transfers ownership of the `Float32Array` to main thread.
6. Main thread reads from `Float32Array` and updates its `Flocker` and `ForceField` state data.
7. `Flocker` and `ForceField` data is now ready to be read from `window.fl.config.FLOCKERS` and `window.fl.config.FORCE_FIELDS` for rendering.

## Usage

```javascript
// 1. Update window.fl.config to desired state.
for (let i = 0; i < 10; i++) {
  const flockerConfig = {
    width: 8,
    height: 8,
    position: { x: Math.random() * 500, y: Math.random() * 250 },
    velocity: { x: (Math.random() * 2) - 1, y: (Math.random() * 2) - 1 },
  };

  const flocker = new window.fl.Flocker(flockerConfig);
  window.fl.config.FLOCKERS.push(flocker);
}

// 2. Add ready callback handler
const readyCallback = function(success) {
  if (success) {
    window.fl.start();
  } else {
    // System is not ready, try setting window.fl.config.logging
    // to true and checking the console.
  }
};

// 3. Add setConfig callback handler
const setConfigCallback = function(success) {
  if (success) {
    window.fl.ready(readyCallback);
  } else {
    // Something went wrong setting the config,
    // try setting window.fl.config.logging to true
    // and checking the console.
  }
};

// 4. (Optional) Add update callback handler
const updateCallback = function() {
  // This is called every time the system updates,
  // you can do your rendering here or set up your own rendering loop.

  // Flocker and ForceField data is available from 
  // window.fl.config.FLOCKERS and window.fl.config.FORCE_FIELDS
};

// 5. Start the setup chain
window.fl.setConfig(setConfigCallback, updateCallback);
```

## API

### window.fl.setConfig(setConfigCallback, updateCallback)

`setConfig` should be called when you have updated `window.fl.config` to its desired state. On calling `setConfig` the framework will read from `window.fl.config` and begin initialising.

- `setConfigCallback` - This function will be called when the initialisation process is complete. It will be called with a boolean that indicates if the config setting process was successful.
- `updateCallback` - This function will be called whenever an update cycle of the framework completes. At this point the `Flocker` and `ForceField` data will have been updated. We recommend you have a render loop that is independent of this callback so your frame rate is not limited by the update cycle speed.

```javascript
const setConfigCallback = function(success) {
  if (success) {
    // Config set successfully.
  } else {
    // Something went wrong setting the config,
    // try setting window.fl.config.logging to true
    // and checking the console.
  }
};

const updateCallback = function() {
  // This is called every time the system updates,
  // you can do your rendering here or set up your own rendering loop.

  // Flocker and ForceField data is available from 
  // window.fl.config.FLOCKERS and window.fl.config.FORCE_FIELDS
};

window.fl.setConfig(setConfigCallback, updateCallback);
```

### window.fl.ready(readyCallback)

`ready` should be called after `setConfig` to ensure that the framework is ready to begin.

- `readyCallback` - This function will be called when the ready check process is complete. It will be called with a boolean that indicates if the framework is ready.

```javascript
const readyCallback = function(success) {
  if (success) {
    // System is ready.
  } else {
    // System is not ready, try setting window.fl.config.logging
    // to true and checking the console.
  }
};

window.fl.ready(readyCallback);
```

### window.fl.start(startCallback)

`start` should be called after `ready` to begin the simulation.

- `startCallback` - This function will be called when the start process is complete. It will be called with a boolean that indicates if the start process was successful.

```javascript
const startCallback = function(success) {
  if (success) {
    // System has started.
  } else {
    // System did not start, try setting window.fl.config.logging
    // to true and checking the console.
  }
};

window.fl.start(startCallback);
```

### window.fl.stop(stopCallback)

`start` should be called after `start` to stop the simulation.

- `stopCallback` - This function will be called when the stop process is complete. It will be called with a boolean that indicates if the stop process was successful.

```javascript
const stopCallback = function(success) {
  if (success) {
    // System has stopped.
  } else {
    // System did not stop, try setting window.fl.config.logging
    // to true and checking the console.
  }
};

window.fl.stop(stopCallback);
```

### window.fl.reset(resetCallback)

`reset` should be called after `stop` to reset the simulation back to the original config state set by `setConfig`.

- `resetCallback` - This function will be called when the reset process is complete. It will be called with a boolean that indicates if the reset process was successful.

```javascript
const resetCallback = function(success) {
  if (success) {
    // System has reset.
  } else {
    // System did not reset, try setting window.fl.config.logging
    // to true and checking the console.
  }
};

window.fl.reset(resetCallback);
```

## Classes

### window.fl.Flocker

The `Flocker` class represents the state of an individual Flocker in the simulation.

- `width` - The width of the Flocker.
- `height` - The height of the Flocker.
- `position` - The position of the Flocker.
- `velocity` - The velocity of the Flocker.
- `acceleration` - The acceleration of the Flocker that will be applied to its velocity.
- `alignment` - The alignment force that will be used in calculating acceleration.
- `cohesion` - The cohesion force that will be used in calculating acceleration.
- `separation` - The separation force that will be used in calculating acceleration.
- `forceField` - The force applied by neighbouring ForceFields that will be used in calculating acceleration.
- `bucket` -  The x and y bucket the Flocker is in. This is used to make neighbour calculations more efficient.
- `rotation` -  The rotation of the Flocker in Radians.

```javascript
const flockerConfig = {
  width: 8,
  height: 8,
  position: { x: Math.random() * 500, y: Math.random() * 250 },
  velocity: { x: (Math.random() * 2) - 1, y: (Math.random() * 2) - 1 },
  acceleration: { x: 0, y: 0 },
  alignment: { x: 0, y: 0 },
  cohesion: { x: 0, y: 0 },
  separation: { x: 0, y: 0 },
  forceField: { x: 0, y: 0 },
  bucket: { x: 0, y: 0 },
  rotation: 0,
};

const flocker = new window.fl.Flocker(flockerConfig);
window.fl.config.FLOCKERS.push(flocker);
```

### window.fl.ForceField

The `ForceField` class represents the state of an individual ForceField in the simulation. A ForceField is an object that applies an attracting or repelling force to any Flockers within its radius.

- `position` - The position of the ForceField.
- `diameter` - The diameter of the ForceField.
- `strength` - The strength of the force to apply to any neighbouring Flockers. Positive strengths will repel Flockers and negative strengths will attract Flockers.

```javascript
const forceFieldConfig = {
  position: {
    x: Math.random() * 500,
    y: Math.random() * 250
  },
  diameter: Math.random() * 50 + 50,
  strength: Math.random() * -0.05
};

const forceField = new window.fl.ForceField(config);
window.fl.config.FORCE_FIELDS.push(forceField);
```

## Config

All configuration data is stored within `window.fl.config`.

```javascript
window.fl.config
```

### Radius settings 

The neighbour and separation radius are used when calculating the forces that apply to a Flocker. The neighbour radius is used to determines which Flockers should be considered neighbours whereas the separation radius is used to determine which Flockers a Flocker will try to move away from.

```javascript
window.fl.config.NEIGHBOUR_RADIUS
window.fl.config.SEPARATION_RADIUS
```

### Weight settings

These settings affect the strengths of the their relative forces on the acceleration of a Flocker.

```javascript
window.fl.config.ALIGNMENT_WEIGHT
window.fl.config.COHESION_WEIGHT
window.fl.config.SEPARATION_WEIGHT
window.fl.config.FORCE_FIELD_WEIGHT
```

### Max force settings

These settings control the maximum strength of the forces that can affect acceleration of a Flocker.

```javascript
window.fl.config.MAX_ALIGNMENT_MAGNITUDE
window.fl.config.MAX_COHESION_MAGNITUDE
window.fl.config.MAX_SEPARATION_MAGNITUDE
window.fl.config.MAX_FORCE_FIELD_MAGNITUDE
```

The max velocity and acceleration settings controls the maximum velocity and acceleration of a Flocker.

```javascript
window.fl.config.MAX_ACCELERATION
window.fl.config.MAX_VELOCITY
```

### World settings

The world width and height determine the bounds within which the simulation takes place. World wrapping determines if a Flocker should be teleported to the other side of the world when leaving the defined world bounds.

```javascript
window.fl.config.WORLD_WIDTH
window.fl.config.WORLD_HEIGHT
window.fl.config.WORLD_WRAP
```

The bucket width and height separate the world into chunks to make the neighbour calculations of a Flocker more efficient. A Flocker will only check for neighbours in its surrounding buckets.

```javascript
window.fl.config.BUCKET_WIDTH
window.fl.config.BUCKET_HEIGHT
```

### Flocker settings

An array storing the Flockers in the simulation.

```javascript
window.fl.config.FLOCKERS
```

### ForceField settings

An array storing the ForceFields in the simulation.

```javascript
window.fl.config.FORCE_FIELDS
```

### Update settings

The maximum number of updates that should take place per second.

```javascript
window.fl.config.UPDATES_PER_SECOND
```

### Logging settings

A boolean indicating whether logging should be enabled.

```javascript
window.fl.config.LOGGING
```

### Default settings

```javascript
window.fl.config: {
  NEIGHBOUR_RADIUS: 20,
  SEPARATION_RADIUS: 20,

  ALIGNMENT_WEIGHT: 0.25,
  COHESION_WEIGHT: 0.45,
  SEPARATION_WEIGHT: 1.5,
  FORCE_FIELD_WEIGHT: 1,

  MAX_ALIGNMENT_MAGNITUDE: 1,
  MAX_COHESION_MAGNITUDE: 1,
  MAX_SEPARATION_MAGNITUDE: 1,
  MAX_FORCE_FIELD_MAGNITUDE: 1,
  
  MAX_ACCELERATION: 0.15,
  MAX_VELOCITY: 1,

  WORLD_WIDTH: 800,
  WORLD_HEIGHT: 400,
  WORLD_WRAP: true,
  BUCKET_WIDTH: 80,
  BUCKET_HEIGHT: 40,
  
  FLOCKERS: [],
  FORCE_FIELDS: [],
  
  UPDATES_PER_SECOND: 30,
  
  LOGGING: false,
};
```