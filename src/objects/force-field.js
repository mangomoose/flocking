function ForceField(config) {
  this.position = config.position || { x: 0, y: 0 };
  this.diameter = config.diameter || 0;
  this.strength = config.strength || 0;
}

export default ForceField;