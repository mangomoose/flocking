function Flocker(config) {
  this.width = config.width || 10;
  this.height = config.height || 10;
  this.position = config.position || { x: 0, y: 0 };
  this.velocity = config.velocity || { x: 0, y: 0 };
  this.acceleration = config.acceleration || { x: 0, y: 0 };
  this.alignment = config.alignment || { x: 0, y: 0 };
  this.cohesion = config.cohesion || { x: 0, y: 0 };
  this.separation = config.separation || { x: 0, y: 0 };
  this.forceField = config.forceField || { x: 0, y: 0 };
  this.bucket = config.bucket || { x: 0, y: 0 };
  this.rotation = config.rotation || 0;
}

export default Flocker;