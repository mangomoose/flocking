import { setupBinaryData, writeToBinary, readFromBinary } from './utils/binary';
import Flocker from './objects/flocker';
import ForceField from './objects/force-field';
import FlockingWorker from 'worker-loader?{"name":"worker.js","inline":true,"fallback":false}!./worker/index.js';

let binaryMetaData = null;
let binaryData = null;

function handleWorkerMessage(event) {
  switch(event.data.command) {
    case 'set-config':
      if (window.fl.config.LOGGING) {
        console.log(`|set-config| command response: ${event.data.data}`);
      }
      window.fl.callbacks.setConfig(event.data.data);
      break;
    case 'ready':
      if (window.fl.config.LOGGING) {
        console.log(`|ready| command response: ${event.data.data}`);
      }
      window.fl.callbacks.ready(event.data.data);
      break;
    case 'start':
      if (window.fl.config.LOGGING) {
        console.log(`|start| command response: ${event.data.data}`);
      }
      window.fl.callbacks.start(event.data.data);
      break;
    case 'stop':
      if (window.fl.config.LOGGING) {
        console.log(`|stop| command response: ${event.data.data}`);
      }
      window.fl.callbacks.stop(event.data.data);
      break;
    case 'reset':
      if (window.fl.config.LOGGING) {
        console.log(`|reset| command response: ${event.data.data}`);
      }
      window.fl.callbacks.reset(event.data.data);
      break;
    case 'binary-meta-data':
      if (window.fl.config.LOGGING) {
        console.log(`|binary-meta-data| command response: ${event.data.data}`);
      }
      break;
    case 'binary-data':
      binaryData = new Float32Array(event.data.data);
      readFromBinary(window.fl.config, binaryMetaData, binaryData);
      window.fl.callbacks.update();
      worker.postMessage({ command: 'binary-data', data: binaryData.buffer }, [binaryData.buffer]);
      break;
  }
}

const worker = new FlockingWorker();
worker.onmessage = handleWorkerMessage;

window.fl = {
  config: {
    NEIGHBOUR_RADIUS: 20,
    SEPARATION_RADIUS: 20,

    ALIGNMENT_WEIGHT: 0.25,
    COHESION_WEIGHT: 0.45,
    SEPARATION_WEIGHT: 1.5,
    FORCE_FIELD_WEIGHT: 1,

    MAX_ALIGNMENT_MAGNITUDE: 1,
    MAX_COHESION_MAGNITUDE: 1,
    MAX_SEPARATION_MAGNITUDE: 1,
    MAX_FORCE_FIELD_MAGNITUDE: 1,

    MAX_ACCELERATION: 0.15,
    MAX_VELOCITY: 1,

    WORLD_WIDTH: 800,
    WORLD_HEIGHT: 400,
    WORLD_WRAP: true,

    BUCKET_WIDTH: 80,
    BUCKET_HEIGHT: 40,
    
    FLOCKERS: [],
    FORCE_FIELDS: [],

    UPDATES_PER_SECOND: 30,

    LOGGING: false,
  },
  callbacks: {
    setConfig: function() {},
    update: function() {},
    ready: function() {},
    start: function() {},
    stop: function() {},
    reset: function() {},
  },
  Flocker: Flocker,
  ForceField: ForceField,
  setConfig: function(setConfigCallback, updateCallback) {
    if (setConfigCallback) {
      window.fl.callbacks.setConfig = setConfigCallback;
    }
    if (updateCallback) {
      window.fl.callbacks.update = updateCallback;
    }

    const { metaData, data } = setupBinaryData(window.fl.config);
    binaryMetaData = metaData;
    binaryData = data;

    writeToBinary(window.fl.config, binaryMetaData, binaryData);

    worker.postMessage({ command: 'set-config', data: window.fl.config });
    worker.postMessage({ command: 'binary-meta-data', data: binaryMetaData });
    worker.postMessage({ command: 'binary-data', data: binaryData.buffer }, [binaryData.buffer]);
  },
  ready: function(callback) {
    if (callback) {
      window.fl.callbacks.ready = callback;
    }
    worker.postMessage({ command: 'ready' });
  },
  start: function(callback) {
    if (callback) {
      window.fl.callbacks.start = callback;
    }
    worker.postMessage({ command: 'start' });
  },
  stop: function(callback) {
    if (callback) {
      window.fl.callbacks.stop = callback;
    }
    worker.postMessage({ command: 'stop' });
  },
  reset: function(callback) {
    if (callback) {
      window.fl.callbacks.reset = callback;
    }
    worker.postMessage({ command: 'reset' });
  }
}