import { Vector, calculateDistance, calculateRotation } from '../utils/maths';

function computeFlockerAlignment(neighbours, config) {
  const alignmentVector = { x: 0, y: 0 };

  if (neighbours.length === 0) {
    return alignmentVector;
  }

  for (let index = 0; index < neighbours.length; index++) {
    Vector.add(alignmentVector, neighbours[index].velocity);
  }

  Vector.normalize(alignmentVector);

  if (Vector.getMagnitude(alignmentVector) > config.MAX_ALIGNMENT_MAGNITUDE) {
    Vector.setMagnitude(alignmentVector, config.MAX_ALIGNMENT_MAGNITUDE);
  }

  return alignmentVector;
}

function computeFlockerCohesion(flocker, neighbours, config) {
  const cohesionVector = { x: 0, y: 0 };

  if (neighbours.length === 0) {
    return cohesionVector;
  }

  for (let index = 0; index < neighbours.length; index++) {
    Vector.add(cohesionVector, neighbours[index].position);
  }
  Vector.divide(cohesionVector, neighbours.length);
  Vector.subtract(cohesionVector, flocker.position);
  Vector.normalize(cohesionVector);

  if (Vector.getMagnitude(cohesionVector) > config.MAX_COHESION_MAGNITUDE) {
    Vector.setMagnitude(cohesionVector, config.MAX_COHESION_MAGNITUDE);
  }

  return cohesionVector;
}

function computeFlockerSeparation(flocker, neighbours, config) {
  const separationVector = { x: 0, y: 0 };
  const filteredNeighbours = [];
  
  if (neighbours.length === 0) {
    return separationVector;
  }

  for (let index = 0; index < neighbours.length; index++) {
    const neighbour = neighbours[index];

    const moveAwayVector = {
      x: flocker.position.x - neighbour.position.x,
      y: flocker.position.y - neighbour.position.y
    };

    const distance = Vector.getMagnitude(moveAwayVector);
    if (distance > config.SEPARATION_RADIUS) {
      continue;
    }

    const weight = (config.SEPARATION_RADIUS - distance) / config.SEPARATION_RADIUS;
    Vector.normalize(moveAwayVector);
    Vector.multiply(moveAwayVector, weight);

    Vector.add(separationVector, moveAwayVector);

    filteredNeighbours.push(neighbour);
  }
  
  if (filteredNeighbours.length === 0) {
    return separationVector;
  }

  if (Vector.getMagnitude(separationVector) > config.MAX_SEPARATION_MAGNITUDE) {
    Vector.setMagnitude(separationVector, config.MAX_SEPARATION_MAGNITUDE);
  }

  return separationVector;
}

function computeFlockerForceField(flocker, forceFields, config) {
  const forceFieldVector = { x: 0, y: 0 };

  for (let index = 0; index < forceFields.length; index++) {
    const forceField = forceFields[index];

    const forceVector = {
      x: flocker.position.x - forceField.position.x,
      y: flocker.position.y - forceField.position.y
    };

    const distance = Vector.getMagnitude(forceVector);
    const tooClose = distance === 0;
    const tooFar = distance > forceField.diameter / 2;
    if (tooClose || tooFar) {
      continue;
    }

    Vector.multiply(forceVector, forceField.strength);
    Vector.add(forceFieldVector, forceVector);
  }

  if (Vector.getMagnitude(forceFieldVector) > config.MAX_FORCE_FIELD_MAGNITUDE) {
    Vector.setMagnitude(forceFieldVector, config.MAX_FORCE_FIELD_MAGNITUDE);
  }

  return forceFieldVector;
}

function findFlockerNeighbours(flocker, flockers, config) {
  return flockers.filter(currentFlocker => {
    if (currentFlocker === flocker) {
      return false;
    }

    const xBucketDiff = Math.abs(currentFlocker.bucket.x - flocker.bucket.x);
    if (xBucketDiff > 1) {
      return false;
    }

    const yBucketDiff = Math.abs(currentFlocker.bucket.y - flocker.bucket.y);
    if (yBucketDiff > 1) {
      return false;
    }
    
    const xDiff = Math.abs(currentFlocker.position.x - flocker.position.x);
    if (xDiff > config.NEIGHBOUR_RADIUS * 1.5) {
      return false;
    }

    const yDiff = Math.abs(currentFlocker.position.y - flocker.position.y);
    if (yDiff > config.NEIGHBOUR_RADIUS * 1.5) {
      return false;
    }

    const distance = calculateDistance(currentFlocker.position, flocker.position);
    const tooClose = distance === 0;
    if (tooClose) {
      return false;
    }
    const tooFar = distance > config.NEIGHBOUR_RADIUS;
    if (tooFar) {
      return false;
    }
    
    return true;
  });
}

export function computeFlocker(flocker, flockers, forceFields, config) {
  const neighbours = findFlockerNeighbours(flocker, flockers, config);

  flocker.alignment = computeFlockerAlignment(neighbours, config);
  flocker.cohesion = computeFlockerCohesion(flocker, neighbours, config);
  flocker.separation = computeFlockerSeparation(flocker, neighbours, config);
  flocker.forceField = computeFlockerForceField(flocker, forceFields, config);

  Vector.multiply(flocker.alignment, config.ALIGNMENT_WEIGHT);
  Vector.multiply(flocker.cohesion, config.COHESION_WEIGHT);
  Vector.multiply(flocker.separation, config.SEPARATION_WEIGHT);
  Vector.multiply(flocker.forceField, config.FORCE_FIELD_WEIGHT);
}

export function updateFlocker(flocker, config) {
  Vector.subtract(flocker.acceleration, flocker.acceleration);
  Vector.add(flocker.acceleration, flocker.alignment);
  Vector.add(flocker.acceleration, flocker.cohesion);
  Vector.add(flocker.acceleration, flocker.separation);
  Vector.add(flocker.acceleration, flocker.forceField);
  if (Vector.getMagnitude(flocker.acceleration) > config.MAX_ACCELERATION) {
    Vector.setMagnitude(flocker.acceleration, config.MAX_ACCELERATION);
  }
  
  Vector.add(flocker.velocity, flocker.acceleration);
  if (Vector.getMagnitude(flocker.velocity) > config.MAX_VELOCITY) {
    Vector.setMagnitude(flocker.velocity, config.MAX_VELOCITY);
  }
  
  Vector.add(flocker.position, flocker.velocity);
  
  if (flocker.position.x < 0) {
    flocker.position.x = config.WORLD_WRAP ? config.WORLD_WIDTH : 0;
  }
  if (flocker.position.x > config.WORLD_WIDTH) {
    flocker.position.x = config.WORLD_WRAP ? 0 : config.WORLD_WIDTH;
  }
  if (flocker.position.y < 0) {
    flocker.position.y = config.WORLD_WRAP ? config.WORLD_HEIGHT : 0;
  }
  if (flocker.position.y > config.WORLD_HEIGHT) {
    flocker.position.y = config.WORLD_WRAP ? 0 : config.WORLD_HEIGHT;
  }

  flocker.bucket.x = Math.floor(flocker.position.x / config.BUCKET_WIDTH);
  flocker.bucket.y = Math.floor(flocker.position.y / config.BUCKET_HEIGHT);

  flocker.rotation = calculateRotation(flocker.velocity, true);
}