import { setupBinaryData, writeToBinary, readFromBinary } from '../utils/binary';
import { computeFlocker, updateFlocker } from './logic';

let originalConfig = null;
let config = null;
let flockers = [];
let forceFields = [];

let binaryMetaData = null;
let binaryData = null

let configSet = false;
let running = false;

function update() {
  if (!running || binaryMetaData === null || binaryData === null) {
    return;
  }
  
  for (let i = 0; i < config.FLOCKERS.length; i++) {
    computeFlocker(config.FLOCKERS[i], config.FLOCKERS, config.FORCE_FIELDS, config);
  }

  for (let i = 0; i < config.FLOCKERS.length; i++) {
    updateFlocker(config.FLOCKERS[i], config);
  }
  
  writeToBinary(config, binaryMetaData, binaryData);
  postMessage({ command: 'binary-data', data: binaryData.buffer }, [binaryData.buffer]);
  binaryData = null;
}

const loop = () => {
  update();
  setTimeout(loop, 1000 / config.UPDATES_PER_SECOND);
};

function handleMessage(event) {
  switch(event.data.command) {
    case 'set-config':
      originalConfig = JSON.parse(JSON.stringify(event.data.data));
      config = JSON.parse(JSON.stringify(event.data.data));
      postMessage({ command: 'set-config', data: true });
      break;
    case 'ready':
      if (config && !running) {
        postMessage({ command: 'ready', data: true });
      } else {
        postMessage({ command: 'ready', data: false });
      }
      break;
    case 'start':
      if (config && !running) {
        running = true;
        loop();
        postMessage({ command: 'start', data: true });
      } else {
        postMessage({ command: 'start', data: false });
      }
      break;
    case 'stop':
      if (config && running) {
        running = false;
        postMessage({ command: 'stop', data: true });
      } else {
        postMessage({ command: 'stop', data: false });
      }
      break;
    case 'reset':
      if (config && !running) {
        running = false;
        config = JSON.parse(JSON.stringify(originalConfig));
        postMessage({ command: 'reset', data: true });
      } else {
        postMessage({ command: 'reset', data: false });
      }
      break;
    case 'binary-meta-data':
      binaryMetaData = event.data.data;
      postMessage({ command: 'binary-meta-data', data: true });
      break;
    case 'binary-data':
      binaryData = new Float32Array(event.data.data);
      if (config && binaryMetaData !== null && binaryData !== null) {
        readFromBinary(config, binaryMetaData, binaryData);
      }
      break;
  }
}

self.onmessage = handleMessage;
