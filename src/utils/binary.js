export function setupBinaryData(config) {
  const forceFieldsCount = config.FORCE_FIELDS.length;
  const forceFieldsValues = config.FORCE_FIELDS.length > 0 ? Object.values(config.FORCE_FIELDS[0]) : [];
  const forceFieldsValuesLength = forceFieldsValues ? forceFieldsValues.length : 0;
  let forceFieldsKeysCount = 0;
  for (let i = 0; i < forceFieldsValuesLength; i++) {
    const value = forceFieldsValues[i];
    
    if (typeof value === 'object') {
      forceFieldsKeysCount += 2;
    } else {
      forceFieldsKeysCount += 1;
    }
  }
  const forceFieldsFloats = forceFieldsCount * forceFieldsKeysCount;

  const flockersCount = config.FLOCKERS.length;
  const flockersValues = config.FLOCKERS.length > 0 ? Object.values(config.FLOCKERS[0]) : [];
  const flockersValuesLength = flockersValues ? flockersValues.length : 0;
  let flockersKeysCount = 0;
  for (let i = 0; i < flockersValuesLength; i++) {
    const value = flockersValues[i];
    
    if (typeof value === 'object') {
      flockersKeysCount += 2;
    } else {
      flockersKeysCount += 1;
    }
  }
  const flockersFloats = flockersCount * flockersKeysCount;
  
  const binaryMetaData = {};
  binaryMetaData.forceFieldsCountLoc = 0;
  binaryMetaData.forceFieldsFloatsCount = forceFieldsFloats;
  binaryMetaData.forceFieldsKeysCount = forceFieldsKeysCount;
  binaryMetaData.forceFieldsStartLoc = binaryMetaData.forceFieldsCountLoc + 1;
  binaryMetaData.forceFieldsEndLoc = binaryMetaData.forceFieldsStartLoc + forceFieldsFloats - 1;
  binaryMetaData.flockersCountLoc = binaryMetaData.forceFieldsEndLoc + 1;
  binaryMetaData.flockersFloatsCount = flockersFloats;
  binaryMetaData.flockersKeysCount = flockersKeysCount;
  binaryMetaData.flockersStartLoc = binaryMetaData.flockersCountLoc + 1;
  binaryMetaData.flockersEndLoc = binaryMetaData.flockersStartLoc + flockersFloats - 1;
  const binaryData = new Float32Array(1 + (forceFieldsFloats) + 1 + (flockersFloats));

  return {
    metaData: binaryMetaData,
    data: binaryData,
  }
}

export function writeToBinary(config, binaryMetaData, binaryData) {
  binaryData[binaryMetaData.forceFieldsCountLoc] = config.FORCE_FIELDS.length;
  for (let i = 0; i < config.FORCE_FIELDS.length; i++) {
    const forceField = config.FORCE_FIELDS[i];
    
    binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 0] = forceField.position.x;
    binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 1] = forceField.position.y;
    binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 2] = forceField.diameter;
    binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 3] = forceField.strength;
  }

  binaryData[binaryMetaData.flockersCountLoc] = config.FLOCKERS.length;
  for (let i = 0; i < config.FLOCKERS.length; i++) {
    const flocker = config.FLOCKERS[i];
    
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 0] = flocker.width;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 1] = flocker.height;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 2] = flocker.position.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 3] = flocker.position.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 4] = flocker.velocity.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 5] = flocker.velocity.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 6] = flocker.acceleration.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 7] = flocker.acceleration.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 8] = flocker.alignment.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 9] = flocker.alignment.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 10] = flocker.cohesion.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 11] = flocker.cohesion.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 12] = flocker.separation.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 13] = flocker.separation.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 14] = flocker.forceField.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 15] = flocker.forceField.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 16] = flocker.bucket.x;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 17] = flocker.bucket.y;
    binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 18] = flocker.rotation;
  }
}

export function readFromBinary(config, binaryMetaData, binaryData) {
  for (let i = 0; i < config.FORCE_FIELDS.length; i++) {
    const forceField = config.FORCE_FIELDS[i];
    
    forceField.position.x = binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 0];
    forceField.position.y = binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 1];
    forceField.diameter = binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 2];
    forceField.strength = binaryData[binaryMetaData.forceFieldsStartLoc + (i * binaryMetaData.forceFieldsKeysCount) + 3];
  }

  for (let i = 0; i < config.FLOCKERS.length; i++) {
    const flocker = config.FLOCKERS[i];
  
    flocker.width = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 0]
    flocker.height = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 1]
    flocker.position.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 2]
    flocker.position.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 3]
    flocker.velocity.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 4]
    flocker.velocity.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 5]
    flocker.acceleration.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 6]
    flocker.acceleration.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 7]
    flocker.alignment.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 8]
    flocker.alignment.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 9]
    flocker.cohesion.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 10]
    flocker.cohesion.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 11]
    flocker.separation.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 12]
    flocker.separation.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 13]
    flocker.forceField.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 14]
    flocker.forceField.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 15]
    flocker.bucket.x = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 16]
    flocker.bucket.y = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 17]
    flocker.rotation = binaryData[binaryMetaData.flockersStartLoc + (i * binaryMetaData.flockersKeysCount) + 18]
  }
}