export const Vector = {
  add: function(vector1, vector2) {
    vector1.x += vector2.x;
    vector1.y += vector2.y;
    return vector1;
  },
  
  subtract: function(vector1, vector2) {
    vector1.x -= vector2.x;
    vector1.y -= vector2.y;
    return vector1;
  },
  
  multiply: function(vector, factor) {
    vector.x *= factor;
    vector.y *= factor;
    return vector;
  },
  
  divide: function(vector, factor) {
    vector.x /= factor;
    vector.y /= factor;
    return vector;
  },
  
  getMagnitude: function(vector) {
    return Math.sqrt((vector.x * vector.x) + (vector.y * vector.y));
  },
  
  setMagnitude: function(vector, magnitude) {
    Vector.multiply(vector, magnitude / Vector.getMagnitude(vector));
  },
  
  normalize: function(vector) {
    const magnitude = Vector.getMagnitude(vector);
    Vector.divide(vector, magnitude);
    return vector;
  },
}

export function calculateDistance(vector1, vector2) {
  const distanceVector = { x: 0, y: 0 };
  distanceVector.x = vector1.x - vector2.x;
  distanceVector.y = vector1.y - vector2.y;
  
  return Vector.getMagnitude(distanceVector);
}

export function calculateRotation(vector, inRadians) {
  const baseVector = { x: 0, y: -1 };
  const directionVector = Vector.normalize({ x: vector.x, y: vector.y });
  const dotProduct = (baseVector.x * directionVector.x) + (baseVector.y * directionVector.y);
  const rotationRadians = Math.acos(dotProduct);

  if (inRadians) {
    return rotationRadians + (directionVector.x < 0 ? ((2 * Math.PI) - (2 * rotationRadians)): 0);;
  }
  else {
    const rotationDegrees = (rotationRadians * 180) / Math.PI;
    return rotationDegrees;
  }
}